namespace MVCMusicStore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlbumColumn : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Albums", "ReleaseDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Albums", "Genre", c => c.String(nullable: false, maxLength: 30));
            AlterColumn("dbo.Albums", "Title", c => c.String(nullable: false, maxLength: 30));
            AlterColumn("dbo.Artists", "Name", c => c.String(nullable: false, maxLength: 30));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Artists", "Name", c => c.String());
            AlterColumn("dbo.Albums", "Title", c => c.String());
            DropColumn("dbo.Albums", "Genre");
            DropColumn("dbo.Albums", "ReleaseDate");
        }
    }
}
