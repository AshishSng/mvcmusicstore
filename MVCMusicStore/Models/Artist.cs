﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MVCMusicStore.Models
{
    public class Artist
    {
        public int ArtistID { get; set; }
        
        [Required]
        [Display(Name = "Artist Name")]
        [StringLength(30,MinimumLength = 2)]
        public string Name { get; set; }

        public List<Album> Albums { get; set; }
    }
    
}