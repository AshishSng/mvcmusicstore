﻿using System.ComponentModel.DataAnnotations;
using System.Data.Entity;

namespace MVCMusicStore.Models
{
    public class Review
    {
        public int ReviewID { get; set; }

        public int AlbumID { get; set; }

        public virtual Album Album { get; set; }

        public string Contents { get; set; }

        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
        public string ReviewEmail { get; set; }
    }

    
}