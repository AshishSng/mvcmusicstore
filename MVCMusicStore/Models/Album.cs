﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;

namespace MVCMusicStore.Models
{
    public class Album
    {
        public int AlbumID { get; set; }

        [Required]
        [StringLength(30,MinimumLength = 1)]
        public string Title { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}",ApplyFormatInEditMode = true)]
        public DateTime ReleaseDate { get; set; }

        public virtual Artist Artist { get; set; }

        [Required]
        [StringLength(maximumLength: 30)]
        public string Genre { get; set; }

        public virtual List<Review> Review { get; set; }
    }
}
   